## Albus prototype

Projet généré par [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Privacy

Ceci est un prototype, toutes les données sont mockées, aucun nom n'a été pris des TPG.

## Développement

Lancer `npm install` pour installer node_modules (nécessite l'installation de node)

Puis lancer `ng serve` pour commencer à développer en local (nécessite l'installation de ng)

## Construction d'une image docker

Il y a 2 dockerfiles :

- Dockerfile : pour le lancer : `docker build -t albus-prototype .`, ce dockerfile nécessite d'avoir au préalable lancé `ng build` ou `npm run build` afin de générer le dossier dist, qui contiendra l'application Angular buildée

- DockerfileBuildAngular : pour le lancer : `docker build -f DockerfileBuildAngular -t albus-prototype-image .`, ce dockerfile ne nécessite pas l'installation de node, il s'occupe lui-même d'installer npm, d'installer les node_modules et de construire l'application Angular

Ces dockerfiles installent un serveur nginx qui servira statiquement l'application Angular sur le port 80

## Lancement de l'image docker

# Image buildée localement

Pour lancer l'image sur le port 80, il faut lancer la commande : `docker run -p 80:80 --name albus-prototype -d albus-prototype-image`
Il faut bien sûr au préalable avoir construit l'image localement, et l'avoir appelée comme précisé dans l'étape au dessus.

# Image récupérée depuis docker hub

Nous avons push l'image sur le docker hub, il est donc possible de la lancer directement, sans avoir à la builder localement, voici la commande : `docker run -p 80:80 --name albus-prototype -d thibautsoriano/albus-prototype`

# Accéder au site

Après avoir lancé l'image, vous pouvez aller sur localhost, et vous accéderez au prototype.
