//Install express server
const express = require('express');
const path = require('path');
// const bodyParser = require('body-parser');
// const cors = require('cors');


const app = express();

// app.use(bodyParser.json({ limit: '5mb', extended: true }));
// app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

// serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/albus-prototype'));

// allowing cors
// app.use(cors());

// it HAS to be LAST element
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/albus-prototype/index.html'));
});

app.listen(process.env.PORT || 8080);