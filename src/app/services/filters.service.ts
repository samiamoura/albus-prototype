import { Injectable } from '@angular/core';

@Injectable()
export class FiltersService {
  displayMode: string = 'module';
  // for the trainer filters : see only his formations or see the unattributed ones
  onlyMyFormations = true;

  setTrainerDisplayModeOnlyMyFormations(onlyMyFormations: boolean): void {
    this.onlyMyFormations = onlyMyFormations;
  }

  setDisplayModeModule() {
    this.displayMode = 'module';
  }

  setDisplayModeParcours() {
    this.displayMode = 'parcours';
  }

  setDisplayModeChemin(): void {
    this.displayMode = 'chemin';
  }

  computeCheminColor(): string {
    if (this.displayMode === 'chemin') {
      return 'primary';
    }

    return 'accent';
  }

  computeParcoursColor(): string {
    if (this.displayMode === 'parcours') {
      return 'primary';
    }

    return 'accent';
  }

  computeModuleColor(): string {
    if (this.displayMode === 'module') {
      return 'primary';
    }

    return 'accent';
  }

  computeTodayFilterColor(): string {
    if (this.displayMode === 'module') {
      return 'primary';
    }

    return 'accent';
  }

  computeTwoWeeksFilterColor(): string {
    if (this.displayMode === 'parcours') {
      return 'primary';
    }

    return 'accent';
  }

  computeMonthFilterColor(): string {
    if (this.displayMode === 'chemin') {
      return 'primary';
    }

    return 'accent';
  }

  computeOnlyMyFormationsFilterColor(): string {
    if (this.onlyMyFormations) {
      return 'primary';
    }

    return 'accent';
  }

  computeUnattributedFormationsFilterColor(): string {
    if (!this.onlyMyFormations) {
      return 'primary';
    }

    return 'accent';
  }
}
