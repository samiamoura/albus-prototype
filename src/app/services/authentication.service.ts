import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {
  connectedAs: string;
  currentUserName: string;

  constructor(private router: Router) {}

  logout(): void {
    this.router.navigate(['/']);
    this.connectedAs = null;
  }

  loginAsAdmin(adminName: string): void {
    this.connectedAs = 'admin';
    this.currentUserName = adminName;
  }

  loginAsTrainer(trainerName: string): void {
    this.connectedAs = 'trainer';
    this.currentUserName = trainerName;
  }
}
