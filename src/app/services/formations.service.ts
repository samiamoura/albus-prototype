import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
// import { mockFormations, mockPlanning } from '../../mocks/mockFormations';
import { Chemin, Module, Parcours } from '../classes/formations';
import { DatesUtilService } from './dates-util.service';

// const AUTOBUS_FORMATION_ID = 0;
// const TROLLEYBUS_FORMATION_ID = 1;

@Injectable()
export class FormationsService {
  mockAdminPlanning: Chemin[] = [];
  // mockAdminPlanning: Chemin[] = mockAdminPlanning();
  displayAutobusFormations = true;
  displayTrolleybusFormations = true;
  // minimalStartDate: Date = new Date(2020, 2, 30);
  mockModules$: Subject<Module[]> = new BehaviorSubject([]);
  mockParcours$: Subject<Parcours[]> = new BehaviorSubject([]);
  mockChemins$: Subject<Chemin[]> = new BehaviorSubject([]);

  constructor(private dateUtilService: DatesUtilService) {}

  getAdminPlanningModulesForDay(givenDate: Date): Observable<Module[]> {
    const modulesList: Module[] = [];
    const daysRangeToCover: Date[] = [new Date(givenDate.getTime())];

    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        for (let module of parcours.modulesList) {
          if (module.isDurationHours) {
            if (this.dateUtilService.daysBetween(module.startDate, givenDate) === 0) {
              modulesList.push(module);
            }
          } else {
            // accepté si date aujourd'hui >= module.startDate && date d'aujourd'hui <= (module.startDate + module.duration)
            // const memoryCopyOfModuleEndDate: Date = this.dateUtilService.addDays(
            //   new Date(module.startDate.getTime()),
            //   module.duration
            // );
            // if (
            //   this.dateUtilService.daysBetween(module.startDate, givenDate) >= 0 &&
            //   this.dateUtilService.daysBetween(givenDate, memoryCopyOfModuleEndDate) >= 0
            // ) {
            //   modulesList.push(module);
            // }
            const daysCoveredByModule: Date[] = this.dateUtilService.getDaysCoveredByModule(module);
            // if (module.name === 'Pratique B au D') {
            //   console.log('daysCoveredByModule', daysCoveredByModule);
            //   console.log('daysRangeToCover', daysRangeToCover);
            // }
            if (this.dateUtilService.isAtLeastOneDayInBothArrays(daysRangeToCover, daysCoveredByModule)) {
              modulesList.push(module);
            }
          }
        }
      }
    }

    // simulate asynchronous
    setTimeout(() => {
      this.mockModules$.next(modulesList);
    });

    return this.mockModules$;
  }

  // if you want parcours for today + 14 days => numberOfDaysUntil should be 14
  getAdminPlanningParcoursUntilNumberOfDays(numberOfDaysUntil: number): Observable<Parcours[]> {
    const parcoursList: Parcours[] = [];
    const daysRangeToCover: Date[] = this.dateUtilService.getDaysRangeFromDate(new Date(), numberOfDaysUntil);

    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        for (let module of parcours.modulesList) {
          if (module.isDurationHours) {
            const daysBetweenModuleAndToday: number = this.dateUtilService.daysBetween(new Date(), module.startDate);
            if (daysBetweenModuleAndToday >= 0 && daysBetweenModuleAndToday <= numberOfDaysUntil) {
              parcoursList.push(parcours);
              break;
            }
          } else {
            // date de fin plus tard que (aujourd'hui + numberofdaysuntil)
            // et date de début du module ne doit pas être supérieure à (aujourd'hui + numberofdausuntil)
            // const memoryCopyOfModuleEndDate: Date = this.dateUtilService.addDays(new Date(module.startDate.getTime()), module.duration);
            // const memoryCopyeOfModuleStartDatePlusNumberOfDaysUntil: Date = this.dateUtilService.addDays(
            //   new Date(module.startDate.getTime()),
            //   numberOfDaysUntil
            // );
            // const todayPlusNumberOfDaysUntil: Date = this.dateUtilService.addDays(new Date(), numberOfDaysUntil);

            // if (
            //   this.dateUtilService.daysBetween(memoryCopyeOfModuleStartDatePlusNumberOfDaysUntil, memoryCopyOfModuleEndDate) >= 0 &&
            //   this.dateUtilService.daysBetween(module.startDate, todayPlusNumberOfDaysUntil) >= 0
            // ) {
            //   parcoursList.push(parcours);
            //   break;
            // }

            // accepted if at least one module is covering at least one day between today and (today + numberOfDaysUntil)
            const daysCoveredByModule: Date[] = this.dateUtilService.getDaysCoveredByModule(module);
            if (this.dateUtilService.isAtLeastOneDayInBothArrays(daysRangeToCover, daysCoveredByModule)) {
              parcoursList.push(parcours);
              break;
            }
          }
        }
      }
    }

    setTimeout(() => {
      this.mockParcours$.next(parcoursList);
    });

    return this.mockParcours$;
  }

  getAdminPlanningCheminsUntilNumberOfDays(numberOfDaysUntil: number): Observable<Chemin[]> {
    const cheminsList: Chemin[] = [];

    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        if (this.moduleDateUntil(parcours, numberOfDaysUntil)) {
          cheminsList.push(chemin);
          break;
        }
      }
    }

    setTimeout(() => {
      this.mockChemins$.next(cheminsList);
    });

    return this.mockChemins$;
  }

  moduleDateUntil(parcours: Parcours, numberOfDaysUntil: number): boolean {
    const daysRangeToCover: Date[] = this.dateUtilService.getDaysRangeFromDate(new Date(), 31);

    for (let module of parcours.modulesList) {
      if (module.isDurationHours || (module.duration === 1 && !module.isDurationHours)) {
        const daysBetweenModuleAndToday: number = this.dateUtilService.daysBetween(new Date(), module.startDate);
        if (daysBetweenModuleAndToday >= 0 && daysBetweenModuleAndToday <= numberOfDaysUntil) {
          return true;
        }
      } else {
        const daysCoveredByModule: Date[] = this.dateUtilService.getDaysCoveredByModule(module);
        if (this.dateUtilService.isAtLeastOneDayInBothArrays(daysRangeToCover, daysCoveredByModule)) {
          return true;
        }
      }
      // const daysBetweenModuleAndToday: number = this.dateUtilService.daysBetween(new Date(), module.startDate);
      // if (daysBetweenModuleAndToday >= 0 && daysBetweenModuleAndToday <= numberOfDaysUntil) {
      //   return true;
      // }
    }

    return false;
  }

  computeParcoursStartDate(parcours: Parcours): Date {
    let currentStartDate: Date;

    for (let module of parcours.modulesList) {
      if (!currentStartDate) {
        currentStartDate = module.startDate;
      } else {
        if (this.dateUtilService.daysBetween(module.startDate, currentStartDate) > 0) {
          currentStartDate = module.startDate;
        }
      }
    }

    return currentStartDate;
  }

  getCheminByModuleId(moduleId: number): Chemin {
    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        for (let module of parcours.modulesList) {
          if (moduleId === module.id) {
            return chemin;
          }
        }
      }
    }

    return null;
  }

  getCheminByParcoursId(parcoursId: number): Chemin {
    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        if (parcoursId === parcours.id) {
          return chemin;
        }
      }
    }

    return null;
  }

  getCheminById(cheminId: number): Chemin {
    for (let chemin of this.mockAdminPlanning) {
      if (cheminId === chemin.id) {
        return chemin;
      }
    }

    return null;
  }

  getParcoursById(parcoursId: number): Parcours {
    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        if (parcours.id === parcoursId) {
          return parcours;
        }
      }
    }

    return null;
  }

  getTrainersForGivenChemin(chemin: Chemin): string[] {
    const trainersList: string[] = [];

    for (let parcours of chemin.parcoursList) {
      for (let module of parcours.modulesList) {
        if (!trainersList.includes(module.trainer)) {
          trainersList.push(module.trainer);
        }
      }
    }

    return trainersList;
  }

  generateTooltipText(chemin: Chemin): string {
    let resText = '';

    for (let learner of chemin.learners) {
      if (resText.length > 0) {
        resText += '\n';
      }
      resText += learner;
    }

    return resText;
  }

  editModule(moduleId: number, trainer: string, moduleStartDate: Date, comment: string): void {
    const moduleToModify: Module = this.getModuleByModuleId(moduleId);

    if (moduleToModify) {
      moduleToModify.trainer = trainer;
      moduleToModify.startDate = moduleStartDate;
      moduleToModify.comment = comment;
    }
  }

  getModuleByModuleId(moduleId: number): Module {
    for (let chemin of this.mockAdminPlanning) {
      for (let parcours of chemin.parcoursList) {
        for (let module of parcours.modulesList) {
          if (moduleId === module.id) {
            return module;
          }
        }
      }
    }

    return null;
  }

  updateLearnersForChemin(chemin: Chemin, learners: string[]): void {
    // console.log('learners i got', learners);

    const addedLearners: string[] = this.computeAddedLearners(chemin.learners, learners);
    // console.log('added learners', addedLearners);
    addedLearners.map((currentLearner) => this.addLearner(chemin, currentLearner));

    const deletedLearners: string[] = this.computeDeletedLearners(chemin.learners, learners);

    // console.log('deleted learners', deletedLearners);
    deletedLearners.map((currentLearner) => this.deleteLearner(chemin, currentLearner));
    chemin.learners = learners;
    // const notModifiedLearners
  }

  addLearner(chemin: Chemin, learner: string): void {
    if (chemin.hasExam) {
      chemin.exams.push({ learnerName: learner, examDate: undefined });
    }
    for (let parcours of chemin.parcoursList) {
      if (parcours.hasExam) {
        parcours.exams.push({ learnerName: learner, examDate: undefined });
      }
    }
  }

  deleteLearner(chemin: Chemin, learner: string): void {
    for (let i = 0; i < chemin.exams.length; i++) {
      if (chemin.exams[i].learnerName === learner) {
        chemin.exams.splice(i, 1);
        break;
      }
    }

    for (let parcours of chemin.parcoursList) {
      for (let i = 0; i < parcours.exams.length; i++) {
        if (parcours.exams[i].learnerName === learner) {
          parcours.exams.splice(i, 1);
          break;
        }
      }
    }
  }

  computeAddedLearners(oldLearners: string[], newLearners: string[]): string[] {
    const res: string[] = [];

    for (let newLearner of newLearners) {
      if (!oldLearners.includes(newLearner)) {
        res.push(newLearner);
      }
    }

    return res;
  }

  computeDeletedLearners(oldLearners: string[], newLearners: string[]): string[] {
    const res: string[] = [];

    for (let oldLearner of oldLearners) {
      if (!newLearners.includes(oldLearner)) {
        res.push(oldLearner);
      }
    }

    return res;
  }

  setExamForChemin(cheminId: number, learnerName: string, reviewer: string, newDate: Date): void {
    const cheminToEdit: Chemin = this.getCheminById(cheminId);

    for (let exam of cheminToEdit.exams) {
      if (exam.learnerName === learnerName) {
        exam.examDate = newDate;
        exam.reviewer = reviewer;
      }
    }
  }

  setExamForParcours(parcoursId: number, learnerName: string, reviewer: string, newDate: Date): void {
    const parcoursToEdit: Parcours = this.getParcoursById(parcoursId);

    for (let exam of parcoursToEdit.exams) {
      if (exam.learnerName === learnerName) {
        exam.examDate = newDate;
        exam.reviewer = reviewer;
      }
    }
  }

  planFormation(formationType: string, formationTeamName: string, formationStartDate: Date): void {
    const newId: number = this.getNewFormationId(this.mockAdminPlanning);
    let currentHighestParcoursId: number = this.getHighestParcoursId(this.mockAdminPlanning);
    let currentHighestModuleId: number = this.getHighestModuleId(this.mockAdminPlanning);
    let currentHighestDate: Date = new Date(formationStartDate.getTime());

    const newChemin: Chemin = {
      id: newId,
      name: formationType,
      teamName: `${formationTeamName} ${newId}`,
      maxLearners: 3,
      learners: [],
      startDate: formationStartDate,
      hasExam: true,
      exams: [],
      parcoursList: [
        {
          id: ++currentHighestParcoursId,
          name: 'Théorie D',
          hasExam: true,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'Théorie D',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 7,
              isDurationHours: false
            }
          ]
        },
        {
          id: ++currentHighestParcoursId,
          name: 'Pratique B au D + connaissances métier',
          hasExam: true,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'Pratique B au D',
              startDate: this.dateUtilService.addDays(currentHighestDate, 8),
              duration: 22,
              isDurationHours: false
            },
            {
              id: ++currentHighestModuleId,
              name: 'SAEIV',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Signal T',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: "Prescriptions en cas d'accidents",
              startDate: new Date(currentHighestDate.getTime()),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Tactiléo',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Caisse de pension',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Syndicat SEV',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Informations Certification',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Syndicat ASIP',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Syndicat Transfair',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Vidéo surveillance',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'RAG 2000',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Management',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Intranet',
              startDate: this.dateUtilService.addDays(currentHighestDate, 2),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Visite RCT',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 4,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'CTT',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'SR',
              startDate: new Date(currentHighestDate.getTime()),
              duration: 1,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'PMR',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Infos RH',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Accueil clientèle',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 2,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Tarification',
              startDate: this.dateUtilService.addDays(currentHighestDate, 3),
              duration: 4,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Planification',
              startDate: this.dateUtilService.addDays(currentHighestDate, 3),
              duration: 4,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'APOP',
              startDate: this.dateUtilService.addDays(currentHighestDate, 2),
              duration: 4,
              isDurationHours: true
            },
            {
              id: ++currentHighestModuleId,
              name: 'Certification conducteur',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 1,
              isDurationHours: true
            }
          ]
        },
        {
          id: ++currentHighestParcoursId,
          name: 'Spécificités AB',
          hasExam: true,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'Spécificités AB',
              startDate: this.dateUtilService.addDays(currentHighestDate, 2),
              duration: 5,
              isDurationHours: false
            }
          ]
        },
        {
          id: ++currentHighestParcoursId,
          name: 'Formation TOSA',
          hasExam: false,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'Formation TOSA',
              startDate: this.dateUtilService.addDays(currentHighestDate, 6),
              duration: 1,
              isDurationHours: false
            }
          ]
        },
        {
          id: ++currentHighestParcoursId,
          name: 'CAP écrit',
          hasExam: true,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'CAP écrit',
              startDate: this.dateUtilService.addDays(currentHighestDate, 1),
              duration: 9,
              isDurationHours: false
            }
          ]
        },
        {
          id: ++currentHighestParcoursId,
          name: 'CAP combiné',
          hasExam: true,
          exams: [],
          modulesList: [
            {
              id: ++currentHighestModuleId,
              name: 'CAP combiné',
              startDate: this.dateUtilService.addDays(currentHighestDate, 10),
              duration: 7,
              isDurationHours: false
            }
          ]
        }
      ]
    };

    // const newChemin: Chemin = {
    //   id: newId,
    //   name: formationType,
    //   maxLearners: 3,
    //   teamName: `Conduite autobus ${newId}`,
    //   learners: [],
    //   startDate: formationStartDate,
    //   hasExam: true,
    //   exams: [],
    //   parcoursList: [
    //     {
    //       id: ++currentHighestParcoursId,
    //       name: 'Théorie D',
    //       hasExam: true,
    //       exams: [],
    //       modulesList: [
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Définition des véhicules',
    //           startDate: new Date(currentHighestDate.getTime())
    //         },
    //         { id: ++currentHighestModuleId, name: 'OAC permis', startDate: this.dateUtilService.addDays(currentHighestDate, 1) },
    //         { id: ++currentHighestModuleId, name: 'Poids et mesures', startDate: this.dateUtilService.addDays(currentHighestDate, 1) },
    //         { id: ++currentHighestModuleId, name: 'OTR 1', startDate: this.dateUtilService.addDays(currentHighestDate, 1) },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Chargement passagers',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         },
    //         { id: ++currentHighestModuleId, name: 'Moteur', startDate: this.dateUtilService.addDays(currentHighestDate, 1) }
    //       ]
    //     },
    //     {
    //       id: ++currentHighestParcoursId,
    //       name: 'Conduite AB',
    //       hasExam: true,
    //       exams: [],
    //       modulesList: [
    //         {
    //           id: ++currentHighestModuleId,
    //           name: "Préparation à l'arrêt",
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 2)
    //         },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Connaissances techniques AB',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Manipulation volant / regard',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Rétroviseurs indicateurs',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         }
    //       ]
    //     },
    //     {
    //       id: ++currentHighestParcoursId,
    //       name: 'CAP écrit',
    //       hasExam: false,
    //       exams: [],
    //       modulesList: [
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Transport de lignes',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 2)
    //         },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Chargement des bagages',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         },
    //         { id: ++currentHighestModuleId, name: 'Guide de voyage', startDate: this.dateUtilService.addDays(currentHighestDate, 1) }
    //       ]
    //     },
    //     {
    //       id: ++currentHighestParcoursId,
    //       name: 'CAP combiné',
    //       hasExam: false,
    //       exams: [],
    //       modulesList: [
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Montage des chaînes',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         },
    //         {
    //           id: ++currentHighestModuleId,
    //           name: 'Préparation au voyage',
    //           startDate: this.dateUtilService.addDays(currentHighestDate, 1)
    //         }
    //       ]
    //     }
    //   ]
    // };

    // console.log('planning', this.mockAdminPlanning);
    this.mockAdminPlanning.push(newChemin);
    this.getAdminPlanningModulesForDay(new Date());
    this.getAdminPlanningParcoursUntilNumberOfDays(14);
    this.getAdminPlanningCheminsUntilNumberOfDays(30);
  }

  getNewFormationId(chemins: Chemin[]): number {
    let res = 0;

    for (let chemin of chemins) {
      if (chemin.id > res) {
        res = chemin.id;
      }
    }

    res++;

    return res;
  }

  getHighestParcoursId(chemins: Chemin[]): number {
    let res = 0;

    for (let chemin of chemins) {
      for (let parcours of chemin.parcoursList) {
        if (parcours.id > res) {
          res = parcours.id;
        }
      }
    }

    return res;
  }

  getHighestModuleId(chemins: Chemin[]): number {
    let res = 0;

    for (let chemin of chemins) {
      for (let parcours of chemin.parcoursList) {
        for (let module of parcours.modulesList) {
          if (module.id > res) {
            res = module.id;
          }
        }
      }
    }

    return res;
  }
}
