import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Chemin, Module, Parcours } from '../classes/formations';
import { FormationsService } from './formations.service';

@Injectable()
export class TrainersService {
  mockModulesForOnlyGivenTrainerForToday$: Subject<Module[]> = new BehaviorSubject([]);
  mockUnattributedModulesForToday$: Subject<Module[]> = new BehaviorSubject([]);
  mockParcoursForOnlyGivenTrainerForTwoWeeks$: Subject<Parcours[]> = new BehaviorSubject([]);
  mockUnattributedParcoursForTwoWeeks$: Subject<Parcours[]> = new BehaviorSubject([]);
  mockCheminsForOnlyGivenTrainerForOneMonth$: Subject<Chemin[]> = new BehaviorSubject([]);
  mockUnattributedCheminsForOneMonth$: Subject<Chemin[]> = new BehaviorSubject([]);

  constructor(private formationsService: FormationsService) {}

  getModulesForTodayAttributedToTrainer(trainerName: string): Observable<Module[]> {
    this.formationsService.getAdminPlanningModulesForDay(new Date()).subscribe((modulesForTodayForAllTrainers) => {
      // console.log('getModulesForTodayAttributedToTrainer sub triggered');
      const modulesForOnlyGivenTrainer: Module[] = [];

      for (let module of modulesForTodayForAllTrainers) {
        if (module.trainer === trainerName) {
          modulesForOnlyGivenTrainer.push(module);
        }
      }

      this.mockModulesForOnlyGivenTrainerForToday$.next(modulesForOnlyGivenTrainer);
    });

    return this.mockModulesForOnlyGivenTrainerForToday$;
  }

  getUnattributedModulesForToday(): Observable<Module[]> {
    this.formationsService.getAdminPlanningModulesForDay(new Date()).subscribe((modulesForTodayForAllTrainers) => {
      const unattributedModules: Module[] = [];

      for (let module of modulesForTodayForAllTrainers) {
        if (module.trainer === '' || !module.trainer) {
          unattributedModules.push(module);
        }
      }

      this.mockUnattributedModulesForToday$.next(unattributedModules);
    });
    return this.mockUnattributedModulesForToday$;
  }

  // if the given trainer is assigned to at least one module of the parcours, then we consider he takes part of the parcours
  getParcoursForTwoWeeksAttributedToTrainer(trainerName: string): Observable<Parcours[]> {
    this.formationsService
      .getAdminPlanningParcoursUntilNumberOfDays(14)
      .subscribe((parcoursForTwoWeeksForAllTrainers) => {
        const parcoursForOnlyGivenTrainer: Parcours[] = [];

        for (let parcours of parcoursForTwoWeeksForAllTrainers) {
          for (let module of parcours.modulesList) {
            if (module.trainer === trainerName) {
              parcoursForOnlyGivenTrainer.push(parcours);
              break;
            }
          }
        }

        this.mockParcoursForOnlyGivenTrainerForTwoWeeks$.next(parcoursForOnlyGivenTrainer);
      });

    return this.mockParcoursForOnlyGivenTrainerForTwoWeeks$;
  }

  // a parcours is considered unattributed as soon as at least one of its modules is unattributed
  getUnattributedParcoursForTwoWeeks(): Observable<Parcours[]> {
    this.formationsService
      .getAdminPlanningParcoursUntilNumberOfDays(14)
      .subscribe((parcoursForTwoWeeksForAllTrainers) => {
        const unattributedParcours: Parcours[] = [];

        for (let parcours of parcoursForTwoWeeksForAllTrainers) {
          for (let module of parcours.modulesList) {
            if (module.trainer === '' || !module.trainer) {
              unattributedParcours.push(parcours);
              break;
            }
          }
        }

        this.mockUnattributedParcoursForTwoWeeks$.next(unattributedParcours);
      });

    return this.mockUnattributedParcoursForTwoWeeks$;
  }

  getCheminsForOneMonthAttributedToTrainer(trainerName: string): Observable<Chemin[]> {
    this.formationsService
      .getAdminPlanningCheminsUntilNumberOfDays(30)
      .subscribe((cheminsForOneMonthForAllTrainers) => {
        const cheminsForOnlyGivenTrainer: Chemin[] = [];

        for (let chemin of cheminsForOneMonthForAllTrainers) {
          for (let parcours of chemin.parcoursList) {
            if (this.isParcoursAttributedToTrainer(parcours, trainerName)) {
              cheminsForOnlyGivenTrainer.push(chemin);
              break;
            }
          }
        }

        this.mockCheminsForOnlyGivenTrainerForOneMonth$.next(cheminsForOnlyGivenTrainer);
      });

    return this.mockCheminsForOnlyGivenTrainerForOneMonth$;
  }

  getUnattributedCheminsForOneMonth(): Observable<Chemin[]> {
    this.formationsService
      .getAdminPlanningCheminsUntilNumberOfDays(30)
      .subscribe((cheminsForOneMonthForAllTrainers) => {
        const unattributedChemins: Chemin[] = [];

        for (let chemin of cheminsForOneMonthForAllTrainers) {
          for (let parcours of chemin.parcoursList) {
            if (this.isParcoursUnattributed(parcours)) {
              unattributedChemins.push(chemin);
              break;
            }
          }
        }

        this.mockUnattributedCheminsForOneMonth$.next(unattributedChemins);
      });

    return this.mockUnattributedCheminsForOneMonth$;
  }

  // returns true if at least one module of the parcours is assigned to the given trainer
  private isParcoursAttributedToTrainer(parcours: Parcours, trainerName: string): boolean {
    for (let module of parcours.modulesList) {
      if (module.trainer === trainerName) {
        return true;
      }
    }

    return false;
  }

  // returns true if at least one module of the parcours is unattributed
  // unattributed means trainer name is empty string or falsy
  private isParcoursUnattributed(parcours: Parcours): boolean {
    for (let module of parcours.modulesList) {
      if (module.trainer === '' || !module.trainer) {
        return true;
      }
    }

    return false;
  }
}
