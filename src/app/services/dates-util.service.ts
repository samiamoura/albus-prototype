import { Injectable } from '@angular/core';
import { Module } from '../classes/formations';

@Injectable()
export class DatesUtilService {
  getDaysCoveredByModule(module: Module): Date[] {
    const res: Date[] = [];

    if (module.isDurationHours) {
      res.push(module.startDate);
    } else {
      for (let i = 0; i < module.duration; i++) {
        const coveredDay: Date = new Date(module.startDate.getTime());

        res.push(this.addDays(coveredDay, i));
      }
    }

    return res;
  }

  // modify by address
  addDays(date: Date, days: number): Date {
    // var result = new Date(date);
    date.setDate(date.getDate() + days);
    return new Date(date.getTime());
  }

  getDaysRangeFromDate(startDate: Date, numberOfDays: number): Date[] {
    const res: Date[] = [];

    for (let i = 0; i < numberOfDays; i++) {
      const coveredDay: Date = new Date(startDate.getTime());

      res.push(this.addDays(coveredDay, i));
    }

    return res;
  }

  isAtLeastOneDayInBothArrays(firstDateArray: Date[], secondDateArray: Date[]): boolean {
    for (let firstDate of firstDateArray) {
      for (let secondDate of secondDateArray) {
        if (this.daysBetween(firstDate, secondDate) === 0) {
          return true;
        }
      }
    }

    return false;
  }

  // if second is later than first, result will be positive
  // if both dates are the same day, regardless time, result will be 0
  daysBetween(first: Date, second: Date): number {
    // Copy date parts of the timestamps, discarding the time parts.
    var one = new Date(first.getFullYear(), first.getMonth(), first.getDate());
    var two = new Date(second.getFullYear(), second.getMonth(), second.getDate());

    // Do the math.
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = two.getTime() - one.getTime();
    var days = millisBetween / millisecondsPerDay;

    // Round down.
    return Math.floor(days);
  }
}
