import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditModuleDialogData } from 'src/app/classes/dialogs-utils';
import { FormationsService } from 'src/app/services/formations.service';

@Component({
  selector: 'app-edit-module-dialog',
  templateUrl: './edit-module-dialog.component.html'
})
export class EditModuleDialogComponent implements OnInit {
  allTrainers: string[] = [undefined, 'Jacques K.', 'Laurent C.', 'Sergio F.', 'Ilyes B.'];
  editModuleForm: FormGroup = new FormGroup({
    trainer: new FormControl(''),
    moduleStartDate: new FormControl(new Date()),
    comment: new FormControl('')
  });

  constructor(
    private formationsService: FormationsService,
    private dialogRef: MatDialogRef<EditModuleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditModuleDialogData
  ) {}

  ngOnInit(): void {
    if (this.data.module.trainer) {
      this.editModuleForm.controls.trainer.setValue(this.data.module.trainer);
    }
    if (this.data.module.startDate) {
      this.editModuleForm.controls.moduleStartDate.setValue(this.data.module.startDate);
    }
    if (this.data.module.comment) {
      this.editModuleForm.controls.comment.setValue(this.data.module.comment);
    }
  }

  onSubmit(): void {
    this.formationsService.editModule(
      this.data.module.id,
      this.editModuleForm.controls.trainer.value,
      this.editModuleForm.controls.moduleStartDate.value,
      this.editModuleForm.controls.comment.value
    );
    this.dialogRef.close(true);
  }
}
