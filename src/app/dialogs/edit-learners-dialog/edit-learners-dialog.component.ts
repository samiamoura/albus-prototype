import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormationsService } from 'src/app/services/formations.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { EditLearnersDialogData } from 'src/app/classes/dialogs-utils';

const MOCK_LEARNERS: string[] = [
  'Nicolas Roussel',
  'Noël Marchand',
  'Michel Dufour',
  'André Blanc',
  'Vincent Boyer',
  'Marie Palot',
  'Gauthier Legrand',
  'Frédéric Masson',
  'Denis Dumont',
  'Benoït Lefèvre',
  'Henri Lambert',
  'Thierry Margoulin',
  'Lucas Rivière'
];

@Component({
  selector: 'app-edit-learners-dialog',
  templateUrl: './edit-learners-dialog.component.html'
})
export class EditLearnersDialogComponent implements OnInit {
  filteredLearners$: Subject<string[]> = new BehaviorSubject<string[]>([]);
  editLearnersForm: FormGroup = new FormGroup({
    learner: new FormControl('', [Validators.required])
  });
  tooManyLearnersError: boolean;
  @ViewChild('learnerInput') learnerInput: ElementRef;
  separatorKeysCodes = [ENTER, COMMA];
  tempLearners: string[];

  constructor(
    private formationsService: FormationsService,
    private dialogRef: MatDialogRef<EditLearnersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditLearnersDialogData
  ) {}

  ngOnInit(): void {
    // console.log('learners', this.data.chemin.learners);
    this.tempLearners = [...this.data.chemin.learners];
    this.filteredLearners$.next(this.computeAvailableLearners(MOCK_LEARNERS, this.tempLearners));
    this.editLearnersForm.controls.learner.valueChanges.subscribe((inputValue) => {
      this.filteredLearners$.next(
        this.filter(inputValue, this.computeAvailableLearners(MOCK_LEARNERS, this.tempLearners))
      );
    });
  }

  computeAvailableLearners(allLearners: string[], alreadySubscribedLearners: string[]): string[] {
    const res: string[] = [];

    for (let learner of allLearners) {
      let found = false;
      for (let alreadySubscribedLearner of alreadySubscribedLearners) {
        if (learner === alreadySubscribedLearner) {
          found = true;
          break;
        }
      }
      if (!found) {
        res.push(learner);
      }
    }

    return res;
  }

  onSubmit(): void {
    this.formationsService.updateLearnersForChemin(this.data.chemin, this.tempLearners);
    this.dialogRef.close(true);
  }

  remove(learner: string): void {
    const index = this.tempLearners.indexOf(learner);

    if (index >= 0) {
      this.tempLearners.splice(index, 1);
    }

    this.tooManyLearnersError = false;
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.tempLearners.length < this.data.chemin.maxLearners) {
      this.tempLearners.push(event.option.viewValue);
      this.learnerInput.nativeElement.value = '';
      this.editLearnersForm.controls.learner.setValue('');
    } else {
      this.tooManyLearnersError = true;
    }
  }

  private filter(value: string, listToFilter: string[]): string[] {
    const filterValue = value.toLowerCase();

    return listToFilter.filter((option) => option.toLowerCase().includes(filterValue));
  }
}
