import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { FormationsService } from 'src/app/services/formations.service';

@Component({
  selector: 'app-plan-formation-dialog',
  templateUrl: './plan-formation-dialog.component.html'
})
export class PlanFormationDialogComponent {
  catalogue: string[] = [
    'Conducteur professionnel AB',
    'Conducteur professionnel TB',
    'Conducteur professionnel TW',
    'Contrôleur de titres de transports',
    'OACP',
    'Opérateur véhicule autonome'
  ];
  planFormationForm: FormGroup = new FormGroup({
    formationType: new FormControl('', [Validators.required]),
    formationStartDate: new FormControl(new Date(), [Validators.required])
  });
  formSubmitted: boolean;

  constructor(
    private formationsService: FormationsService,
    private dialogRef: MatDialogRef<PlanFormationDialogComponent>
  ) {}

  onSubmit(): void {
    this.formSubmitted = true;
    if (this.planFormationForm.valid) {
      if (this.planFormationForm.controls.formationType.value === 'Conducteur professionnel AB') {
        this.formationsService.planFormation(
          this.planFormationForm.controls.formationType.value,
          'Conducteur AB',
          this.planFormationForm.controls.formationStartDate.value
        );
      }

      this.dialogRef.close();
    }
  }
}
