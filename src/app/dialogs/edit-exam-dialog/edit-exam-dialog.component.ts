import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditExamDialogData } from 'src/app/classes/dialogs-utils';
import { FormationsService } from 'src/app/services/formations.service';

@Component({
  selector: 'app-edit-exam-dialog',
  templateUrl: './edit-exam-dialog.component.html'
})
export class EditExamDialogComponent implements OnInit {
  editExamForm: FormGroup = new FormGroup({
    examDate: new FormControl(new Date()),
    reviewer: new FormControl('')
  });

  constructor(
    private formationsService: FormationsService,
    private dialogRef: MatDialogRef<EditExamDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: EditExamDialogData
  ) {}

  ngOnInit(): void {
    // if (this.data.exam.examDate) {
    this.editExamForm.controls.examDate.patchValue(this.data.exam.examDate);
    // }
    this.editExamForm.controls.reviewer.patchValue(this.data.exam.reviewer);
  }

  onSubmit(): void {
    if (this.data.isChemin) {
      this.formationsService.setExamForChemin(
        this.data.elementToModifyId,
        this.data.exam.learnerName,
        this.editExamForm.controls.reviewer.value,
        this.editExamForm.controls.examDate.value
      );
    } else {
      this.formationsService.setExamForParcours(
        this.data.elementToModifyId,
        this.data.exam.learnerName,
        this.editExamForm.controls.reviewer.value,
        this.editExamForm.controls.examDate.value
      );
    }
    this.dialogRef.close(true);
  }
}
