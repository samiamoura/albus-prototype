import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chemin } from '../classes/formations';
import { FormationsService } from '../services/formations.service';

@Component({
  selector: 'app-chemin',
  templateUrl: './chemin.component.html',
  styleUrls: ['./chemin.component.scss']
})
export class CheminComponent implements OnInit {
  @Input() indexNumber: number;
  @Input() chemin: Chemin;
  tooltipText: string;

  constructor(private formationsService: FormationsService, private router: Router) {}

  ngOnInit(): void {
    this.tooltipText = this.formationsService.generateTooltipText(this.chemin);
  }

  navigateToFormationDetails(): void {
    this.router.navigate(['/formation-details', this.chemin.id]);
  }
}
