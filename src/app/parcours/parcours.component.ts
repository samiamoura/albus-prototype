import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chemin, Parcours } from '../classes/formations';
import { FormationsService } from '../services/formations.service';

@Component({
  selector: 'app-parcours',
  templateUrl: './parcours.component.html',
  styleUrls: ['./parcours.component.scss']
})
export class ParcoursComponent implements OnInit {
  @Input() indexNumber: number;
  @Input() parcours: Parcours;
  associatedChemin: Chemin;
  computedParcoursStartDate: Date;
  tooltipText: string;

  constructor(private formationsService: FormationsService, private router: Router) {}

  ngOnInit(): void {
    this.associatedChemin = this.formationsService.getCheminByParcoursId(this.parcours.id);
    this.computedParcoursStartDate = this.formationsService.computeParcoursStartDate(this.parcours);
    this.tooltipText = this.formationsService.generateTooltipText(this.associatedChemin);
  }

  navigateToFormationDetails(): void {
    this.router.navigate(['/formation-details', this.associatedChemin.id]);
  }
}
