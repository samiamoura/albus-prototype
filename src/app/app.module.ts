import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModuleComponent } from './module/module.component';
import { FormationsService } from './services/formations.service';
import { HomeComponent } from './home/home.component';
import { FiltersComponent } from './filters/filters.component';
import { ParcoursComponent } from './parcours/parcours.component';
import { FiltersService } from './services/filters.service';
import { CheminComponent } from './chemin/chemin.component';
import { AuthenticationService } from './services/authentication.service';
import { FormationDetailsComponent } from './formation-details/formation-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlanFormationDialogComponent } from './dialogs/plan-formation-dialog/plan-formation-dialog.component';
import { EditModuleDialogComponent } from './dialogs/edit-module-dialog/edit-module-dialog.component';
import { EditLearnersDialogComponent } from './dialogs/edit-learners-dialog/edit-learners-dialog.component';
import { TrainersService } from './services/trainers.service';
import { LoginComponent } from './login/login.component';
import { PlanningComponent } from './planning/planning.component';
import { EditExamDialogComponent } from './dialogs/edit-exam-dialog/edit-exam-dialog.component';
import { DatesUtilService } from './services/dates-util.service';
import { CheminHeaderComponent } from './chemin-header/chemin-header.component';
import { ModuleHeaderComponent } from './module-header/module-header.component';
import { ParcoursHeaderComponent } from './parcours-header/parcours-header.component';

@NgModule({
  declarations: [
    AppComponent,
    PlanFormationDialogComponent,
    EditModuleDialogComponent,
    EditLearnersDialogComponent,
    EditExamDialogComponent,
    HomeComponent,
    ModuleComponent,
    FiltersComponent,
    ParcoursComponent,
    CheminComponent,
    FormationDetailsComponent,
    LoginComponent,
    PlanningComponent,
    CheminHeaderComponent,
    ModuleHeaderComponent,
    ParcoursHeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [FormationsService, FiltersService, AuthenticationService, TrainersService, DatesUtilService],
  bootstrap: [AppComponent]
})
export class AppModule {}
