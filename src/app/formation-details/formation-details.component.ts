import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EditExamDialogData, EditLearnersDialogData, EditModuleDialogData } from '../classes/dialogs-utils';
import { Chemin, Exam, Module } from '../classes/formations';
import { EditExamDialogComponent } from '../dialogs/edit-exam-dialog/edit-exam-dialog.component';
import { EditLearnersDialogComponent } from '../dialogs/edit-learners-dialog/edit-learners-dialog.component';
import { EditModuleDialogComponent } from '../dialogs/edit-module-dialog/edit-module-dialog.component';
import { AuthenticationService } from '../services/authentication.service';
import { FormationsService } from '../services/formations.service';

@Component({
  selector: 'app-formation-details',
  templateUrl: './formation-details.component.html',
  styleUrls: ['./formation-details.component.scss']
})
export class FormationDetailsComponent implements OnInit {
  chemin: Chemin;
  trainersList: string[];

  constructor(
    private formationsService: FormationsService,
    public authenticationService: AuthenticationService,
    private router: Router,
    private matDialog: MatDialog,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.chemin = this.formationsService.getCheminById(Number(params.get('cheminId')));
    });
    // this.chemin = this.formationsService.cheminForDetails;
    if (this.chemin) {
      this.trainersList = this.formationsService.getTrainersForGivenChemin(this.chemin);
    }
  }

  goToHomePage(): void {
    this.router.navigate(['/']);
  }

  showEditModuleDialog(module: Module) {
    const dialogData: EditModuleDialogData = {
      module: module
    };
    const dialogRef = this.matDialog.open(EditModuleDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.trainersList = this.formationsService.getTrainersForGivenChemin(this.chemin);
      }
    });
  }

  showEditLearnersDialog(chemin: Chemin) {
    const dialogData: EditLearnersDialogData = {
      chemin: chemin
    };
    const dialogRef = this.matDialog.open(EditLearnersDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.chemin = this.formationsService.getCheminById(this.chemin.id);
      }
    });
  }

  showEditExamDialog(elementToModifyId: number, isChemin: boolean, exam: Exam, examName: string) {
    const dialogData: EditExamDialogData = {
      exam: exam,
      examName: examName,
      elementToModifyId: elementToModifyId,
      isChemin: isChemin
    };
    const dialogRef = this.matDialog.open(EditExamDialogComponent, {
      width: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // this.chemin = this.formationsService.getCheminById(this.chemin.id);
      }
    });
  }
}
