import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { FormationsService } from './services/formations.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(public authenticationService: AuthenticationService, private formationsService: FormationsService) {}

  ngOnInit(): void {
    // mock Laurent C.
    this.formationsService.planFormation('Conducteur professionnel AB', 'Conducteur AB', new Date('2020-10-30'));
    this.formationsService.mockAdminPlanning[0].parcoursList[0].modulesList[0].trainer = 'Laurent C.';
    for (let module of this.formationsService.mockAdminPlanning[0].parcoursList[1].modulesList) {
      module.trainer = 'Laurent C.';
    }
    this.formationsService.updateLearnersForChemin(this.formationsService.mockAdminPlanning[0], [
      'Nicolas Roussel',
      'Noël Marchand',
      'Michel Dufour'
    ]);

    // mock Jacques K.
    this.formationsService.planFormation('Conducteur professionnel AB', 'Conducteur AB', new Date('2020-10-31'));
    this.formationsService.mockAdminPlanning[1].parcoursList[0].modulesList[0].trainer = 'Jacques K.';
    for (let module of this.formationsService.mockAdminPlanning[1].parcoursList[1].modulesList) {
      module.trainer = 'Jacques K.';
    }
    this.formationsService.updateLearnersForChemin(this.formationsService.mockAdminPlanning[1], [
      'André Blanc',
      'Vincent Boyer',
      'Marie Palot'
    ]);

    // mock Sergio F.
    this.formationsService.planFormation('Conducteur professionnel AB', 'Conducteur AB', new Date('2020-10-31'));
    this.formationsService.mockAdminPlanning[2].parcoursList[0].modulesList[0].trainer = 'Sergio F.';
    for (let module of this.formationsService.mockAdminPlanning[2].parcoursList[1].modulesList) {
      module.trainer = 'Sergio F.';
    }
    this.formationsService.updateLearnersForChemin(this.formationsService.mockAdminPlanning[2], [
      'Gauthier Legrand',
      'Frédéric Masson',
      'Denis Dumont'
    ]);

    // mock Ilyes B.
    this.formationsService.planFormation('Conducteur professionnel AB', 'Conducteur AB', new Date('2020-10-31'));
    this.formationsService.mockAdminPlanning[3].parcoursList[0].modulesList[0].trainer = 'Ilyes B.';
    for (let module of this.formationsService.mockAdminPlanning[3].parcoursList[1].modulesList) {
      module.trainer = 'Ilyes B.';
    }
    this.formationsService.updateLearnersForChemin(this.formationsService.mockAdminPlanning[3], [
      'Benoït Lefèvre',
      'Henri Lambert',
      'Thierry Margoulin'
    ]);

    // mock empty formation
    this.formationsService.planFormation('Conducteur professionnel AB', 'Conducteur AB', new Date('2020-11-23'));
  }
}
