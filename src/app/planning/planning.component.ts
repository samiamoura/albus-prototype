import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Chemin, Module, Parcours } from '../classes/formations';
import { PlanFormationDialogComponent } from '../dialogs/plan-formation-dialog/plan-formation-dialog.component';
import { AuthenticationService } from '../services/authentication.service';
import { FiltersService } from '../services/filters.service';
import { FormationsService } from '../services/formations.service';
import { TrainersService } from '../services/trainers.service';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html'
})
export class PlanningComponent implements OnInit {
  // admin
  adminModulesMock$: Observable<Module[]>;
  adminParcoursMock$: Observable<Parcours[]>;
  adminCheminsMock$: Observable<Chemin[]>;
  // trainer
  mockModulesForOnlyGivenTrainerForToday$: Observable<Module[]>;
  mockParcoursForOnlyGivenTrainerForTwoWeeks$: Observable<Parcours[]>;
  mockCheminsForOnlyGivenTrainerForOneMonth$: Observable<Chemin[]>;
  mockUnattributedModulesForToday$: Observable<Module[]>;
  mockUnattributedParcoursForTwoWeeks$: Observable<Parcours[]>;
  mockUnattributedCheminsForOneMonth$: Observable<Chemin[]>;

  constructor(
    public formationsService: FormationsService,
    public filtersService: FiltersService,
    private matDialog: MatDialog,
    public authenticationService: AuthenticationService,
    private trainersService: TrainersService
  ) {}

  ngOnInit(): void {
    if (this.authenticationService.connectedAs === 'admin') {
      this.adminModulesMock$ = this.formationsService.getAdminPlanningModulesForDay(new Date());
      this.adminParcoursMock$ = this.formationsService.getAdminPlanningParcoursUntilNumberOfDays(14);
      this.adminCheminsMock$ = this.formationsService.getAdminPlanningCheminsUntilNumberOfDays(30);
    } else {
      this.mockModulesForOnlyGivenTrainerForToday$ = this.trainersService.getModulesForTodayAttributedToTrainer(
        this.authenticationService.currentUserName
      );
      this.mockParcoursForOnlyGivenTrainerForTwoWeeks$ = this.trainersService.getParcoursForTwoWeeksAttributedToTrainer(
        this.authenticationService.currentUserName
      );
      this.mockCheminsForOnlyGivenTrainerForOneMonth$ = this.trainersService.getCheminsForOneMonthAttributedToTrainer(
        this.authenticationService.currentUserName
      );
      this.mockUnattributedModulesForToday$ = this.trainersService.getUnattributedModulesForToday();
      this.mockUnattributedParcoursForTwoWeeks$ = this.trainersService.getUnattributedParcoursForTwoWeeks();
      this.mockUnattributedCheminsForOneMonth$ = this.trainersService.getUnattributedCheminsForOneMonth();
    }
  }

  showPlanFormationPopup() {
    const dialogRef = this.matDialog.open(PlanFormationDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
      }
    });
  }
}
