import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

// const MOCK_MODULES: string[] = [
//   'Définition des véhicules',
//   'OAC permis',
//   'Poids et mesures',
//   'OTR 1',
//   'Chargement passagers',
//   'Moteur',
//   "Préparation à l'arrêt",
//   'Connaissances techniques AB',
//   'Manipulation volant / regard',
//   'Rétroviseurs indicateurs',
//   'Transport de lignes',
//   'Chargement des bagages',
//   'Guide de voyage',
//   'Montage des chaînes',
//   'Préparation au voyage',
//   'Connaissances CITARO',
//   'Connaissances lignes TB',
//   'Perception',
//   'Analyse',
//   'Décision',
//   'Action',
//   'Réseau 600',
//   'Spécificités des lieux',
//   'NAW 2',
//   'Intervenant ligne aérienne'
// ];

// const MOCK_PARCOURS: string[] = [
//   'Théorie D',
//   'Conduite AB',
//   'CAP écrit',
//   'CAP combiné',
//   'Conduite avec AB + lignes TB',
//   'Parcours réseau 600 + connaissances techniques + conduite TB accompagnée'
// ];

// const MOCK_CHEMINS: string[] = ['Conduite autobus', 'Conduite trolleybus', 'Conduite autobus'];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  constructor(public authenticationService: AuthenticationService) {}
}
