import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Chemin, Module } from '../classes/formations';
import { FormationsService } from '../services/formations.service';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss']
})
export class ModuleComponent implements OnInit {
  @Input() indexNumber: number;
  @Input() module: Module;
  associatedChemin: Chemin;
  tooltipText: string;

  constructor(private formationsService: FormationsService, private router: Router) {}

  ngOnInit(): void {
    this.associatedChemin = this.formationsService.getCheminByModuleId(this.module.id);
    this.tooltipText = this.formationsService.generateTooltipText(this.associatedChemin);
  }

  navigateToFormationDetails(): void {
    this.router.navigate(['/formation-details', this.associatedChemin.id]);
  }
}
