export interface Chemin {
  id: number;
  name: string;
  parcoursList: Parcours[];
  learners: string[];
  maxLearners: number;
  teamName?: string;
  startDate?: Date;
  hasExam: boolean;
  exams?: Exam[];
}

export interface Parcours {
  id: number;
  name: string;
  modulesList: Module[];
  hasExam: boolean;
  exams?: Exam[];
}

export interface Module {
  id: number;
  name: string;
  startDate?: Date;
  trainer?: string;
  comment?: string;
  duration: number;
  isDurationHours: boolean;
}

export interface Exam {
  learnerName: string;
  examDate: Date;
  reviewer?: string;
}
