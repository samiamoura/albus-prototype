import { Chemin, Exam, Module, Parcours } from './formations';

export interface EditModuleDialogData {
  module: Module;
}

export interface EditLearnersDialogData {
  chemin: Chemin;
}

export interface EditExamDialogData {
  exam: Exam;
  elementToModifyId: number;
  isChemin: boolean;
  examName: string;
}
