import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { FiltersService } from '../services/filters.service';
import { FormationsService } from '../services/formations.service';

const MOCK_LEARNERS_NAMES: string[] = [
  'Nicolas Roussel',
  'Noël Marchand',
  'Michel Dufour',
  'André Blanc',
  'Vincent Boyer',
  'Marie Palot',
  'Gauthier Legrand',
  'Frédéric Masson',
  'Denis Dumont',
  'Benoït Lefèvre',
  'Henri Lambert',
  'Thierry Margoulin',
  'Lucas Rivière'
];

const MOCK_TRAINERS_NAMES: string[] = ['Jacques K.', 'Laurent C.', 'Sergio F.', 'Ilyes B.'];

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  studentsInputValue: string;
  trainersInputValue: string;
  filteredStudentsNames$: Subject<string[]> = new BehaviorSubject<string[]>(MOCK_LEARNERS_NAMES);
  filteredTrainersNames$: Subject<string[]> = new BehaviorSubject<string[]>(MOCK_TRAINERS_NAMES);

  constructor(
    public formationsService: FormationsService,
    public filtersService: FiltersService,
    public authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {}

  filterStudentsNames(): void {
    this.filteredStudentsNames$.next(this.filter(this.studentsInputValue, MOCK_LEARNERS_NAMES));
  }

  filterTrainersNames(): void {
    this.filteredTrainersNames$.next(this.filter(this.trainersInputValue, MOCK_TRAINERS_NAMES));
  }

  private filter(value: string, listToFilter: string[]): string[] {
    const filterValue = value.toLowerCase();

    return listToFilter.filter((option) => option.toLowerCase().includes(filterValue));
  }
}
