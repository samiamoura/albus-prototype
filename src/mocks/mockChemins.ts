import { Chemin } from 'src/app/classes/formations';

// export const mockAdminPlanning = function (): Chemin[] {
//   return [
//     {
//       id: 0,
//       name: 'Conducteur professionnel AB',
//       teamName: 'Conducteur AB 0',
//       maxLearners: 3,
//       learners: ['André Blanc', 'Vincent Boyer', 'Marie Palot'],
//       startDate: new Date('2020-10-26'),
//       hasExam: false,
//       exams: [],
//       parcoursList: [
//         {
//           id: 0,
//           name: 'Théorie D',
//           hasExam: true,
//           exams: [
//             {
//               learnerName: 'André Blanc',
//               examDate: new Date('2020-11-04')
//             },
//             {
//               learnerName: 'Vincent Boyer',
//               examDate: new Date('2020-11-04')
//             },
//             {
//               learnerName: 'Marie Palot',
//               examDate: new Date('2020-11-04')
//             }
//           ],
//           modulesList: [
//             {
//               id: 0,
//               name: 'Théorie D',
//               startDate: new Date('2020-10-26'),
//               trainer: 'Jean Dupont',
//               duration: 7,
//               isDurationHours: false
//             }
//           ]
//         },
//         {
//           id: 1,
//           name: 'Pratique B au D + connaissances métier',
//           hasExam: true,
//           exams: [
//             {
//               learnerName: 'André Blanc',
//               examDate: new Date('2020-11-26')
//             },
//             {
//               learnerName: 'Vincent Boyer',
//               examDate: new Date('2020-11-26')
//             },
//             {
//               learnerName: 'Marie Palot',
//               examDate: new Date('2020-11-26')
//             }
//           ],
//           modulesList: [
//             {
//               id: 1,
//               name: 'Pratique B au D',
//               startDate: new Date('2020-11-05'),
//               trainer: 'Laurent C.',
//               duration: 22,
//               isDurationHours: false
//             },
//             {
//               id: 2,
//               name: 'SAEIV',
//               startDate: new Date('2020-11-05'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 3,
//               name: 'Signal T',
//               startDate: new Date('2020-11-05'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 4,
//               name: "Prescriptions en cas d'accidents",
//               startDate: new Date('2020-11-05'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 5,
//               name: 'Tactiléo',
//               startDate: new Date('2020-11-05'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 6,
//               name: 'Caisse de pension',
//               startDate: new Date('2020-11-06'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 7,
//               name: 'Syndicat SEV',
//               startDate: new Date('2020-11-06'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 8,
//               name: 'Informations Certification',
//               startDate: new Date('2020-11-07'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 9,
//               name: 'Syndicat ASIP',
//               startDate: new Date('2020-11-07'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 10,
//               name: 'Syndicat Transfair',
//               startDate: new Date('2020-11-08'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 11,
//               name: 'Vidéo surveillance',
//               startDate: new Date('2020-11-09'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 12,
//               name: 'RAG 2000',
//               startDate: new Date('2020-11-09'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 13,
//               name: 'Management',
//               startDate: new Date('2020-11-09'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 14,
//               name: 'Intranet',
//               startDate: new Date('2020-11-10'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 15,
//               name: 'Visite RCT',
//               startDate: new Date('2020-11-12'),
//               trainer: 'Laurent C.',
//               duration: 4,
//               isDurationHours: true
//             },
//             {
//               id: 16,
//               name: 'CTT',
//               startDate: new Date('2020-11-13'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 17,
//               name: 'SR',
//               startDate: new Date('2020-11-13'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             },
//             {
//               id: 18,
//               name: 'PMR',
//               startDate: new Date('2020-11-14'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 19,
//               name: 'Infos RH',
//               startDate: new Date('2020-11-15'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 20,
//               name: 'Accueil clientèle',
//               startDate: new Date('2020-11-16'),
//               trainer: 'Laurent C.',
//               duration: 2,
//               isDurationHours: true
//             },
//             {
//               id: 21,
//               name: 'Tarification',
//               startDate: new Date('2020-11-19'),
//               trainer: 'Laurent C.',
//               duration: 4,
//               isDurationHours: true
//             },
//             {
//               id: 22,
//               name: 'Planification',
//               startDate: new Date('2020-11-22'),
//               trainer: 'Laurent C.',
//               duration: 4,
//               isDurationHours: true
//             },
//             {
//               id: 23,
//               name: 'APOP',
//               startDate: new Date('2020-11-24'),
//               trainer: 'Laurent C.',
//               duration: 4,
//               isDurationHours: true
//             },
//             {
//               id: 24,
//               name: 'Certification conducteur',
//               startDate: new Date('2020-11-25'),
//               trainer: 'Laurent C.',
//               duration: 1,
//               isDurationHours: true
//             }
//           ]
//         },
//         {
//           id: 2,
//           name: 'Spécificités AB',
//           hasExam: true,
//           exams: [
//             {
//               learnerName: 'André Blanc',
//               examDate: new Date('2020-12-02')
//             },
//             {
//               learnerName: 'Vincent Boyer',
//               examDate: new Date('2020-12-02')
//             },
//             {
//               learnerName: 'Marie Palot',
//               examDate: new Date('2020-12-02')
//             }
//           ],
//           modulesList: [
//             {
//               id: 25,
//               name: 'Spécificités AB',
//               startDate: new Date('2020-11-27'),
//               trainer: 'Bernard Martin',
//               duration: 5,
//               isDurationHours: false
//             }
//           ]
//         },
//         {
//           id: 3,
//           name: 'Formation TOSA',
//           hasExam: false,
//           exams: [],
//           modulesList: [
//             {
//               id: 26,
//               name: 'Formation TOSA',
//               startDate: new Date('2020-12-03'),
//               trainer: 'Bernard Martin',
//               duration: 1,
//               isDurationHours: false
//             }
//           ]
//         },
//         {
//           id: 4,
//           name: 'CAP écrit',
//           hasExam: true,
//           exams: [
//             {
//               learnerName: 'André Blanc',
//               examDate: new Date('2020-12-13')
//             },
//             {
//               learnerName: 'Vincent Boyer',
//               examDate: new Date('2020-12-13')
//             },
//             {
//               learnerName: 'Marie Palot',
//               examDate: new Date('2020-12-13')
//             }
//           ],
//           modulesList: [
//             {
//               id: 27,
//               name: 'CAP écrit',
//               startDate: new Date('2020-12-04'),
//               trainer: 'Bernard Martin',
//               duration: 9,
//               isDurationHours: false
//             }
//           ]
//         },
//         {
//           id: 5,
//           name: 'CAP combiné',
//           hasExam: true,
//           exams: [
//             {
//               learnerName: 'André Blanc',
//               examDate: new Date('2020-12-21')
//             },
//             {
//               learnerName: 'Vincent Boyer',
//               examDate: new Date('2020-12-21')
//             },
//             {
//               learnerName: 'Marie Palot',
//               examDate: new Date('2020-12-21')
//             }
//           ],
//           modulesList: [
//             {
//               id: 28,
//               name: 'CAP combiné',
//               startDate: new Date('2020-12-14'),
//               trainer: 'Bernard Martin',
//               duration: 7,
//               isDurationHours: false
//             }
//           ]
//         }
//       ]
//     }
//   ];
// };
