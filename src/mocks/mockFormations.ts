// import { Formation } from '../app/classes/formations';

// export const mockFormations = function (): Formation[] {
//   return [
//     {
//       id: 0,
//       name: 'Conduite autobus',
//       parcoursList: [
//         {
//           id: 0,
//           name: 'Théorie D',
//           modulesList: [
//             { id: 0, name: 'Définition des véhicules' },
//             { id: 1, name: 'OAC permis' },
//             { id: 2, name: 'Poids et mesures' },
//             { id: 3, name: 'OTR 1' },
//             { id: 4, name: 'Chargement passagers' },
//             { id: 5, name: 'Moteur' }
//           ]
//         },
//         {
//           id: 1,
//           name: 'Conduite AB',
//           modulesList: [
//             { id: 6, name: "Préparation à l'arrêt" },
//             { id: 7, name: 'Connaissances techniques AB' },
//             { id: 8, name: 'Manipulation volant / regard' },
//             { id: 9, name: 'Rétroviseurs indicateurs' }
//           ]
//         },
//         {
//           id: 2,
//           name: 'CAP écrit',
//           modulesList: [
//             { id: 10, name: 'Transport de lignes' },
//             { id: 11, name: 'Chargement des bagages' },
//             { id: 12, name: 'Guide de voyage' }
//           ]
//         },
//         {
//           id: 3,
//           name: 'CAP combiné',
//           modulesList: [
//             { id: 13, name: 'Montage des chaînes' },
//             { id: 14, name: 'Préparation au voyage' }
//           ]
//         }
//       ]
//     },
//     {
//       id: 1,
//       name: 'Conduite trolleybus',
//       parcoursList: [
//         {
//           id: 4,
//           name: 'Conduite avec AB + lignes TB',
//           modulesList: [
//             { id: 15, name: 'Connaissances CITARO' },
//             { id: 16, name: 'Connaissances lignes TB' },
//             { id: 17, name: 'Perception' },
//             { id: 18, name: 'Analyse' },
//             { id: 19, name: 'Décision' },
//             { id: 20, name: 'Action' }
//           ]
//         },
//         {
//           id: 5,
//           name: 'Parcours réseau 600 + connaissances techniques + conduite TB accompagnée',
//           modulesList: [
//             { id: 21, name: 'Réseau 600' },
//             { id: 22, name: 'Spécificités des lieux' },
//             { id: 23, name: 'NAW 2' },
//             { id: 24, name: 'Intervenant ligne aérienne' }
//           ]
//         }
//       ]
//     }
//   ];
// };

// export const mockPlanning = function (): Formation[] {
//   return [
//     {
//       id: 0,
//       name: 'Conduite autobus',
//       teamName: 'Equipe A',
//       startDate: new Date('2020-10-26'),
//       parcoursList: [
//         {
//           id: 0,
//           name: 'Théorie D',
//           modulesList: [
//             { id: 0, name: 'Définition des véhicules', startDate: new Date('2020-10-26') },
//             { id: 1, name: 'OAC permis', startDate: new Date('2020-10-27') },
//             { id: 2, name: 'Poids et mesures', startDate: new Date('2020-10-28') },
//             { id: 3, name: 'OTR 1', startDate: new Date('2020-10-29') },
//             { id: 4, name: 'Chargement passagers', startDate: new Date('2020-10-30') },
//             { id: 5, name: 'Moteur', startDate: new Date('2020-11-02') }
//           ]
//         },
//         {
//           id: 1,
//           name: 'Conduite AB',
//           modulesList: [
//             { id: 6, name: "Préparation à l'arrêt", startDate: new Date('2020-11-03') },
//             { id: 7, name: 'Connaissances techniques AB', startDate: new Date('2020-11-04') },
//             { id: 8, name: 'Manipulation volant / regard', startDate: new Date('2020-11-05') },
//             { id: 9, name: 'Rétroviseurs indicateurs', startDate: new Date('2020-11-06') }
//           ]
//         },
//         {
//           id: 2,
//           name: 'CAP écrit',
//           modulesList: [
//             { id: 10, name: 'Transport de lignes', startDate: new Date('2020-11-09') },
//             { id: 11, name: 'Chargement des bagages', startDate: new Date('2020-11-10') },
//             { id: 12, name: 'Guide de voyage', startDate: new Date('2020-11-11') }
//           ]
//         },
//         {
//           id: 3,
//           name: 'CAP combiné',
//           modulesList: [
//             { id: 13, name: 'Montage des chaînes', startDate: new Date('2020-11-12') },
//             { id: 14, name: 'Préparation au voyage', startDate: new Date('2020-11-13') }
//           ]
//         }
//       ]
//     }
//   ];
// };
// // { id: 1, formation: mockFormations()[0], teamName: 'Equipe B', startDate: new Date('2020-10-25') },
// //     { id: 2, formation: mockFormations()[1], teamName: 'Equipe C', startDate: new Date('2020-10-30') },
// //     { id: 3, formation: mockFormations()[1], teamName: 'Equipe D', startDate: new Date('2020-11-18') },
// //     { id: 4, formation: mockFormations()[0], teamName: 'Equipe E', startDate: new Date('2020-11-19') }
